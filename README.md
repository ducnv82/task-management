# Task management application backend uses Spring Boot
The Java backend application should implement the following features:

*   A scheduler should create new tasks at a random interval
*   Tasks should be persisted in database and should have at least the following fields    
    *   id (uuid)
    *   createdAt
    *   updatedAt
    *   dueDate
    *   resolvedAt
    *   title
    *   description
    *   priority
    *   status
    *   (feel free to add additional fields if required)
*   Rest Endpoints for communication with the Frontend
*   For communication with the frontend DTOs should be used.

The frontend application can be served via the backend application

# Prerequisite

1. Installations: Install latest Oracle JDK 8, download at http://www.oracle.com/technetwork/java/javase/downloads/index.html
2. Install PostgreSQL 9.6 

# Run the application

1.  Use the command `mvn package` to generate the executable file
2.  Run the jar file in the folder _target_  with the command `java -DDATASOURCE_URL=[YOUR JDBC URL TO POSTGRESQL] -DDATASOURCE_USERNAME=[YOUR DATABASE USERNAME] -DDATASOURCE_PASSWORD=[YOUR DATABASE PASSWORD] -jar task-management.jar`
  
Note: If you do not provide database information in system properties (or environment variables), it uses the default params for db connection with url **jdbc:postgresql://localhost:5432/postgres**, username: **postgres**, password: **postgres**

In web browser open the page [http://localhost:8080](http://localhost:8080 "Spring Boot app") to access the app.


# Thought about requirements
Requirement: A task can be postponed, so that it

*   is removed from the list
*   appears on the list at a later time again

so I implement: Postponed tasks are hidden in the frontend app except for its **due date** is **today** (appears on the list at a later time again)

Feel free to discuss the changes (if needed) for requirements.
