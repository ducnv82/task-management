package task.management.service;

import java.util.List;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import task.management.model.Task;

public interface TaskService {

    List<Task> findAllTasks();

    ResponseEntity<Task> update(Task task);

    ResponseEntity<Task> findById(UUID id);
}
