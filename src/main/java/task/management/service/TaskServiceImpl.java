package task.management.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import task.management.model.Task;
import task.management.repository.TaskRepository;

import static java.util.stream.Collectors.toList;

@Service
public class TaskServiceImpl implements TaskService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ScheduledExecutorService scheduledExecutorService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Value("${creating.task:true}")
    private boolean creatingTask;

    @PostConstruct
    @Transactional
    public void createTasks() {
        if (creatingTask) {
            final AtomicInteger count = new AtomicInteger(0);
            scheduledExecutorService.scheduleWithFixedDelay(() -> {
                int number = count.getAndIncrement();
                task.management.entity.Task taskEntity = new task.management.entity.Task();
                taskEntity.setDueDate(LocalDate.now().plusDays(number + 1));
                taskEntity.setResolvedAt(LocalDate.now().plusDays(number));
                taskEntity.setTitle("Title " + number);
                taskEntity.setDescription("Description " + number);
                taskEntity.setPriority(number % 2 == 0 ? "HIGH" : "NORMAL");
                taskEntity.setStatus(number % 2 == 0 ? "OPEN" : "POSTPONED");
                taskRepository.save(taskEntity);

                LOGGER.info("A new task has been saved into db");
                simpMessagingTemplate.convertAndSend("/topic/tasks", taskEntity.getId().toString());
            }, 0, 1, TimeUnit.MINUTES);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> findAllTasks() {
        LOGGER.info("findAllTasks");
        return taskRepository.findAll().stream().map(entity -> Task.of(entity)).collect(toList());
    }

    @Override
    @Transactional
    public ResponseEntity<Task> update(final Task task) {
        LOGGER.info("Update Task with id {}", task.getId());

        final Optional<task.management.entity.Task> taskData = taskRepository.findById(task.getId());

        if (taskData.isPresent()) {
            task.management.entity.Task taskEntity = taskData.get();
            taskEntity.setDueDate(task.getDueDate());
            taskEntity.setResolvedAt(task.getResolvedAt());
            taskEntity.setTitle(task.getTitle());
            taskEntity.setDescription(task.getDescription());
            taskEntity.setPriority(task.getPriority());
            taskEntity.setStatus(task.getStatus());
            return ResponseEntity.ok(Task.of(taskRepository.save(taskEntity)));
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<Task> findById(final UUID id) {
        final Optional<task.management.entity.Task> taskData = taskRepository.findById(id);
        return taskData.isPresent() ? ResponseEntity.ok(Task.of(taskData.get())) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
